<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Create rows for users table
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Vladislav Bespalov',
                'email' => 'vlastelinVladislav@yandex.ru',
                'password' => Hash::make('1352aCa1352'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Tirion Lannister',
                'email' => 'tirionLannister@yandex.ru',
                'password' => Hash::make('666aBa666'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => 'Jhon Snow',
                'email' => 'jhonSnow@yandex.ru',
                'password' => Hash::make('555aDa555'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
