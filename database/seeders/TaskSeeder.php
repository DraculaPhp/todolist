<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Create rows for tasks table
 */
class TaskSeeder extends Seeder
{
    /**
     * Get user id by email
     * @param $email
     * @return int
     */
    private function getUserIdByEmail($email): int
    {
        return User::query()->where('email', '=', $email)->first()->id;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
                'title' => 'Пойти на пробежку',
                'description' => 'Пробежать 10 км за 50 минут',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('vlastelinVladislav@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Сделать тестовое задание',
                'description' => 'Написать todo list на laravel 8',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('vlastelinVladislav@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Сходить в магазин',
                'description' => 'Купить продукты к ужину',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('vlastelinVladislav@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Убить Тайвина Ланнистера',
                'description' => 'Застрелить Тайвина из арбалета, когда он будет в ночной комнате',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('tirionLannister@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Заключить выгодное брачное соглашение с Дорном',
                'description' => 'Выдать принцессу Мирцеллу за принца Дорана из Дорна',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('tirionLannister@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Победить Станниса Баратеона при Черноводной',
                'description' => 'Взорвать его флот диким огнём и повести за собой солдат',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('tirionLannister@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Воскреснуть из мёртвых',
                'description' => 'Вернуться с того света, после метяжа при помощи магии красной жрицы Асшая',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('jhonSnow@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Сделать очередную тупость',
                'description' => 'Попытаться достать живого мертвеца, чтобы показать его Серсее и подарить Королю Ночи дракона',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('jhonSnow@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'title' => 'Убить королеву Дайенерис',
                'description' => 'Пырнуть её ножом, после того как она сойдёт с ума и спалит Королевскую Гавань',
                'status' => 0,
                'user_id' => $this->getUserIdByEmail('jhonSnow@yandex.ru'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
        ]);
    }
}
