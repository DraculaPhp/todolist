<h2>Todo List</h2>
<h3>Данное приложение было выполнено 
в рамках тестового задания Framework Team</h3>

<p>Для локального разворачивания требуются настроенные Apache, 
MySQL и установленный PHP версии 7 и выше.</p>

<p>Склонируйте к себе проект командой 
<strong>git clone https://gitlab.com/DraculaPhp/todolist.git</strong></p>

<p>Также обязательно не забудьте выполнить <strong>php artisan key:generate</strong></p>

<p>Затем установите зависимости командой <strong>composer install</strong></p>

<p>Далее необходимо в файле <strong>.env</strong> 
ввести свои данные от базы данных в полях <strong>DB_*</strong></p>

<p>После этого выполните <strong>php artisan migrate</strong></p>

<p>Для того, чтобы наполнить приложение тестовыми данными
выполните команду <strong>php artisan db:seed</strong></p>
