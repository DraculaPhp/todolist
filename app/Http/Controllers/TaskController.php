<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Controller for processing task
 */
class TaskController extends Controller
{
    /**
     * Return form for creating task
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Create task action
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255|min:10',
            'description' => 'required|min:10',
        ]);

        $params = $request->all();
        $params['user_id'] = Auth::user()->id;
        Task::create($params);
        return redirect()->route('home')->withSuccess('Created task: ' . $request->title);
    }

    /**
     * Return detailed view of task
     */
    public function show(Task $task)
    {
        $this->authorize('view', $task);

        return view('tasks.show', compact('task'));
    }

    /**
     * Return form for editing task
     */
    public function edit(Task $task)
    {
        $this->authorize('update', $task);

        return view('tasks.edit', compact('task'));
    }

    /**
     * Update task action
     */
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'title' => 'required|max:255|min:10',
            'description' => 'required|min:10',
        ]);

        $this->authorize('update', $task);

        if (!$request->has('status')) {
            $request['status'] = 0;
        }

        $task->update($request->all());
        return redirect()->route('home')->withSuccess('Updated task: ' . $task->title);
    }

    /**
     * Delete task action
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);

        $task->delete();

        return redirect()->route('home')->withSuccess('Deleted task: ' . $task->title);
    }
}
