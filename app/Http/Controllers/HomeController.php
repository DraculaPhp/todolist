<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $tasksQuery = Task::query();

        if ($request->has('sort_asc')) {
            $tasksQuery->orderBy('created_at');
        }

        if ($request->has('sort_desc')) {
            $tasksQuery->orderByDesc('created_at');
        }

        if ($request->has('state')) {
            if ($request['state'] == 0) {
                $tasksQuery->where('status', '=', '0');
            } elseif ($request['state'] == 1) {
                $tasksQuery->where('status', '=', '1');
            }
        }

        $tasks = $tasksQuery
            ->where('user_id', '=', Auth::user()->id)
            ->paginate(3)
            ->withPath("?" . $request->getQueryString());

        return view('home', compact('tasks'));
    }
}
