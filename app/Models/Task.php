<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Task Model
 */
class Task extends Model
{
    protected $guarded = [
        '_method',
        '_token'
    ];

    use HasFactory;

    /**
     * get the user who owns the task
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
