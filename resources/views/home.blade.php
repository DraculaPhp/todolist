@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-3">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <form method="GET" action="{{ route('home') }}" class="mt-3">
                        <div class="filters row">
                            <div class="col-sm-6 col-md-3">
                                <p>Сортировать по дате:</p>
                                <label for="sort_asc">Сначала старые
                                    <input type="checkbox" name="sort_asc" id="sort_asc" size="6" value="{{ request()->sort_asc }}">
                                </label>
                                <label for="sort_desc">Сначала новые
                                    <input type="checkbox" name="sort_desc" id="sort_desc" size="6"  value="{{ request()->sort_desc }}">
                                </label>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <p>Фильтровать по статусу:</p>
                                <label for="filter_opened">Только открытые задачи
                                    <input type="radio" name="state" id="filter_opened" size="6"  value="0">
                                </label>
                                <label for="filter_closed">Только закрытые задачи
                                    <input type="radio" name="state" id="filter_closed" size="6"  value="1">
                                </label>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <button type="submit" class="btn btn-primary">Применить</button>
                                <a href="{{ route('home') }}" class="btn btn-warning">Сброс</a>
                            </div>
                        </div>
                    </form>
                    <a type="button" class="btn btn-secondary mt-3" href="{{ route('tasks.create') }}"
                       xmlns="http://www.w3.org/1999/html">Create task</a>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered mt-3">
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                        @foreach ($tasks as $task)
                            <tr>
                                <td>{{ $task->title }}</td>
                                <td>{{ $task->description }}</td>
                                <td>{{ $task->created_at }}</td>
                                <td class="input-group">
                                    <a class="btn btn-info" href="{{ route('tasks.show', $task->id) }}">Show</a>
                                    <a class="btn btn-primary" href="{{ route('tasks.edit', $task->id) }}">Edit</a>
                                    <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="mt-3">{{ $tasks->links() }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
