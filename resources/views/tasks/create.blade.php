@extends('layouts.app')
@section('content')
    <a type="button" class="btn btn-secondary mt-3" href="{{ route('home') }}"
       xmlns="http://www.w3.org/1999/html">Back home</a>
    <form action="{{ route('tasks.store') }}" method="POST">
        @csrf
        <div class="input-group row mt-3">
            <label for="title" class="col-sm-2 col-form-label">Title:</label>
            <div class="col-sm-6">
                <input name="title" type="text" class="form-control" placeholder="title" aria-label="title" value="{{ old('title') }}">
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="input-group row mt-3">
            <label for="description" class="col-sm-2 col-form-label">Description:</label>
            <div class="col-sm-6">
                <textarea class="form-control" style="height:150px" name="description" placeholder="Description">
                    {{ old('description') }}
                </textarea>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-4">
            <div class="col">
                <button type="submit" class="btn btn-success">Create</button>
            </div>
        </div>
    </form>
@endsection

