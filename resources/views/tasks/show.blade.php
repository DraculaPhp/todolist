@extends('layouts.app')
@section('content')

    <div class="input-group row mt-3">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Show task №{{ $task->id }}</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('home') }}">Back</a>
            </div>
        </div>
    </div>

    <div class="input-group row mt-3">
        <div class="col-sm-6">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $task->title }}
            </div>
        </div>
    </div>
    <div class="input-group row mt-3">
        <div class="col-sm-6">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $task->description }}
            </div>
        </div>
    </div>
    <div class="input-group row mt-3">
        <div class="col-sm-6">
            <div class="form-group">
                <strong>Time of creation:</strong>
                {{ $task->created_at }}
            </div>
        </div>
    </div>
    <div class="input-group row mt-3">
        <div class="col-sm-6">
            <div class="form-group">
                <strong>Status:</strong>
                @if($task->status == 0)
                    In Progress
                @else
                    Closed
                @endif
            </div>
        </div>
    </div>
@endsection
