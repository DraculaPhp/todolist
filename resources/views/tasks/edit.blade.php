@extends('layouts.app')
@section('content')
    <a type="button" class="btn btn-secondary mt-3" href="{{ route('home') }}"
       xmlns="http://www.w3.org/1999/html">Back home</a>
    <form action="{{ route('tasks.update', $task->id) }}" method="POST">
        @csrf

        @method('PUT')
        <div class="input-group row mt-3">
            <label for="title" class="col-sm-2 col-form-label">Title:</label>
            <div class="col-sm-6">
                <input name="title" type="text" class="form-control" placeholder="title" aria-label="title"
                       value="{{ old('title', $task->title) }}">
                @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="input-group row mt-3">
            <label for="description" class="col-sm-2 col-form-label">Description:</label>
            <div class="col-sm-6">
                <textarea class="form-control" style="height:150px" name="description" placeholder="Description">
                    @isset($task){{ old('description', $task->description) }}@endisset
                </textarea>
                @error('description')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="input-group row mt-3">
            <label for="status" class="col-sm-2 col-form-label">Status:</label>
            <div class="col-sm-6">
                <input id="status" name="status" type="checkbox" placeholder="status" aria-label="status" value="1"
                       @if($task->status) checked @endif>
                @error('status')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-4">
            <div class="col">
                <button type="submit" class="btn btn-success">Update</button>
            </div>
        </div>
    </form>
@endsection
